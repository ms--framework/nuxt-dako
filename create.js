var fs = require("fs");
const capp = require("./ms/app.js");
var clapp = new capp();

const csearch = require("./ms/search.js");
var clsearch = new csearch();

const cadd = require("./ms/add.js");
var cladd = new cadd();

const cview = require("./ms/view.js");
var clview = new cview();

const cmod = require("./ms/mod.js");
var clmod = new cmod();

var nom_module = "";
var reponse = true;
var rep_error = "";

if (!process.argv[2]) {
  reponse = false;
  rep_error = "On ne connait le nom du module";
}

if (reponse == true) {
  //
  nom_module = process.argv[2];
  const clas = require("./api/" + nom_module + "/ms_" + nom_module + ".js");
  var myclass = new clas();
  var mydata = myclass.data();
  var mydata_list = myclass.list();
  //

  clapp.create_doc("./pages/" + nom_module + "");
  //

  // Create search page
  clsearch.genererate(nom_module, mydata_list, mydata);
  //

  // Create ADD page
  cladd.genererate(nom_module, mydata_list, mydata);
  //

  // Create VIEW page
  clview.genererate(nom_module, mydata_list, mydata);
  //

  // Create MOD page
  clmod.genererate(nom_module, mydata_list, mydata);
  //
  console.log(
    ' <nuxt-link to="/' +
      nom_module +
      '/search" class="nav-link">Gestion ' +
      nom_module +
      "</nuxt-link> "
  );
} else {
  console.log(rep_error);
}
