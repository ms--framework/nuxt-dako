var fs = require("fs");
const capp = require("./ms/app.js");
var clapp = new capp();

var reponse = true;
var rep_error = "";

if (!process.argv[2]) {
  reponse = false;
  rep_error = "On ne connait pas le nom du module";
}

if (reponse == true) {
  //
  nom_module = process.argv[2];
  const template_1 = clapp.recup_file("./ms/model/user.js");
  const template_2 = clapp.recup_file("./ms/model/ms_user.js");
  clapp.create_doc("./api/" + nom_module);
  clapp.save_file("./api/" + nom_module + "/" + nom_module + ".js", template_1);
  clapp.save_file(
    "./api/" + nom_module + "/ms_" + nom_module + ".js",
    template_2
  );
} else {
  console.log(rep_error);
}
