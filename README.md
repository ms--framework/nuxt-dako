# Nom de projet
[![Netlify Status](https://api.netlify.com/api/v1/badges/612fe97e-9a2e-47e7-8b4a-f17cf2f599b4/deploy-status)](https://app.netlify.com/sites/optimistic-agnesi-7735f4/deploys) 

MS Framework Fron-End

## Démarrage

### Pré-requis

```
NodeJS
```

### Installation

```
git clone https://gitlab.com/ms--framework/nuxt-dako.git
```

```
npm install
```

```
npm run dev
```

- Supprimer le fichier .GIT pour démarrer votre projet

## Fais

- [NuxtJS](https://nuxtjs.org/) - Front-End Framework
- [Bootstrap](https://getbootstrap.com/) - CSS Framework

## Auteur

- **Cyriaque Ovaga** - _Fondateur_ - [Cabinet CEIPI](https://twitter.com/ovagacyriaque)
