import axios from "axios";

export const state = () => ({
  token: null,
  user: {},
  auth: false,
  // api_url: "http://localhost/dev/registre-information/api-registre-information/"
  api_url: "https://api-registre-information.herokuapp.com/"
});

export const mutations = {
  SET_USER(state, user) {
    state.user = user;
    state.auth = false;
    if (user !== null) {
      state.token = user.token;
      state.auth = true;
    }
  }
};

export const actions = {
  // nuxtServerInit is called by Nuxt.js before server-rendering every page
  nuxtServerInit({ commit }, { req }) {
    let user = null;
    if (req.headers.cookie) {
      //  const parsed = cookieparser.parse(req.headers.cookie);
      try {
        user = JSON.parse(parsed.user);
      } catch (err) {
        // No valid cookie found
      }
    }
    //commit("SET_USER", user);
  },
  async logout({ commit }) {
    commit("SET_USER", null);
    this.$router.push("/login");
  }
};
