var fs = require("fs");

class app {
  constructor(module) {
    this.module = module;
  }
  create_doc(link) {
    fs.mkdir("" + link + "", { recursive: true }, err => {
      if (err) throw err;
    });
  }

  recup_file(link) {
    return fs.readFileSync(link, "utf8");
  }

  save_file(link, doc) {
    fs.writeFile(link, doc, function(err) {
      if (err) throw err;
    });
  }
}

module.exports = app;
