class ms_user {
  data() {
    return [
      {
        id: "nom",
        title: "Nom",
        type: "input",
        required: true,
        type_input: "text",
        placeholder: "Votre nom",
        data_name: "nom"
      },
      {
        id: "mdp",
        title: "Mot de passe",
        type: "input",
        required: true,
        type_input: "password",
        placeholder: "Mot de passe",
        data_name: "mdp"
      },
      {
        id: "email",
        title: "Email",
        type: "input",
        required: true,
        type_input: "email",
        placeholder: "Votre email",
        data_name: "email"
      },
      {
        id: "telephone",
        title: "Téléphone",
        type: "input",
        required: false,
        type_input: "text",
        placeholder: "Votre numéro de téléphone",
        data_name: "telephone"
      },
      {
        id: "adresse",
        title: "Adresse",
        type: "input",
        required: false,
        type_input: "text",
        placeholder: "Votre adresse",
        data_name: "adresse"
      }
    ];
  }

  filter() {
    return [{ id: "nom" }, { id: "email" }, { id: "adresse" }];
  }

  list() {
    return [
      { id: "nom" },
      { id: "email" },
      { id: "telephone" },
      { id: "adresse" }
    ];
  }
}

module.exports = ms_user;
