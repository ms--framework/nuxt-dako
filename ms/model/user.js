import axios from "axios";
var querystring = require("querystring");
var qs = require("qs");

export default class user {
  constructor(store) {
    this.store = store;
    this.api = this.store.state.api_url;
    this.token = this.store.state.token;

    // Script de connection
    this.url_get_list = this.api + "user/user/list/";
    this.url_search = this.api + "user/user/search/";
    this.url_delete = this.api + "user/user/delete/";
    this.url_add = this.api + "user/user/add/";
    this.url_info = this.api + "user/user/info/";
    this.url_mod = this.api + "user/user/mod/";
    //
  }
  async get_list() {
    let url = this.url_get_list;
    const { data } = await axios.get(url);
    if (data.reponse == true) {
      return data.data;
    } else {
      console.log(url);
      throw new Error(data.message);
    }
  }

  async get_call(url) {
    const { data } = await axios.get(this.api + url);
    if (data.reponse == true) {
      return data.data;
    } else {
      console.log(url);
      throw new Error(data.message);
    }
  }

  async mod(datas) {
    let url = this.url_mod + querystring.stringify(datas);
    const { data } = await axios.get(url);
    if (data.reponse == true) {
      return data.data;
    } else {
      console.log(url);
      throw new Error(data.message);
    }
  }

  async get_info(id) {
    let url = this.url_info + id;
    const { data } = await axios.get(url);
    if (data.reponse == true) {
      return data.data;
    } else {
      console.log(url);
      throw new Error(data.message);
    }
  }

  async search(user) {
    let url = this.url_search + querystring.stringify(user);
    // console.log(url);
    const { data } = await axios.get(url);
    if (data.reponse == true) {
      return data.data;
    } else {
      console.log(url);
      throw new Error(data.message);
    }
  }

  async sup(user) {
    let url = this.url_delete + user;
    // console.log(url);
    const { data } = await axios.post(url);
    if (data.reponse == true) {
      return data.data;
    } else {
      console.log(url);
      throw new Error(data.message);
    }
  }

  async add(user) {
    let url = this.url_add + querystring.stringify(user);
    console.log(url);
    const { data } = await axios.post(url);
    if (data.reponse == true) {
      return data.data;
    } else {
      console.log(url);
      throw new Error(data.message);
    }
  }
}
