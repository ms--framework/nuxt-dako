const capp = require("../ms/app.js");
var clapp = new capp();

class search {
  constructor(module) {
    this.module = module;
  }

  genererate(nom_module, mydata_list, mydata) {
    //
    const new_template_1 = clapp.recup_file("./ms/template/add.vue");
    //

    //
    var formulaire = "";
    var extra_val = "";
    var exta_function = "";
    var exta_call_function = "";
    //

    var i = 0;
    for (i = 0; i < mydata.length; i++) {
      formulaire = formulaire + this.generate_html(mydata[i]);

      if (this.generate_extra_data(mydata[i]) != "") {
        if (extra_val == "") {
          extra_val = this.generate_extra_data(mydata[i]);
        } else {
          extra_val = extra_val + "," + this.generate_extra_data(mydata[i]);
        }
      }

      if (this.generate_extra_function(mydata[i]) != "") {
        if (exta_function == "") {
          exta_function = this.generate_extra_function(mydata[i]);
        } else {
          exta_function =
            exta_function + "," + this.generate_extra_function(mydata[i]);
        }
      }

      if (this.generate_extra_call_function(mydata[i]) != "")
        exta_call_function =
          exta_call_function + this.generate_extra_call_function(mydata[i]);
    }

    var new_template_2 = new_template_1.replace("monnomdemodule", nom_module);
    var new_template_3 = new_template_2.replace("monnomdemodule", nom_module);
    var new_template_4 = new_template_3.replace("monnomdemodule", nom_module);
    var new_template_5 = new_template_4.replace("monnomdemodule", nom_module);
    var new_template_5 = new_template_4.replace("monnomdemodule", nom_module);
    var new_template_6 = new_template_5.replace(
      "<formulaire></formulaire>",
      formulaire
    );
    var new_template_7 = new_template_6.replace("else: []", extra_val);
    var new_template_8 = new_template_7.replace(
      "this.standart();",
      exta_call_function
    );
    var new_template_9 = new_template_8.replace(
      "standart: function() {}",
      exta_function
    );

    clapp.save_file("./pages/" + nom_module + "/add.vue", new_template_9);

    //
  }

  generate_extra_data(data) {
    var rep = "";
    if (data.type == "select") {
      rep = " extra_" + data.data_name + ":[] ";
    }

    return rep;
  }

  generate_extra_function(data) {
    var rep = "";
    if (data.type == "select") {
      rep =
        " call_" +
        data.data_name +
        ': async function() {const clas = new Class(this.$store); try { var data = await clas.get_call("' +
        data.url_select +
        '"); this.extra_' +
        data.data_name +
        "=data; } catch (error) { this.error_message = error.message; }} ";
    }

    return rep;
  }

  generate_extra_call_function(data) {
    var rep = "";
    if (data.type == "select") {
      rep = " this.call_" + data.data_name + "();  ";
    }
    return rep;
  }
  generate_html(data) {
    var ma_rep =
      '<div class="form-group"><label for="">' + data.title + "</label>";

    if (data.type == "input") {
      var required = "";
      if (data.required == true) required = "required";
      ma_rep =
        ma_rep +
        '  <input type="' +
        data.type_input +
        '" class="form-control" v-model="data.' +
        data.data_name +
        '" placeholder="' +
        data.placeholder +
        '" ' +
        required +
        "> ";
    }

    if (data.type == "select") {
      var required = "";
      if (data.required == true) required = "required";
      ma_rep =
        ma_rep +
        '  <select class="form-control" v-model="data.' +
        data.data_name +
        '" ' +
        required +
        "> ";
      ma_rep =
        ma_rep +
        ' <option v-for="item in extra_' +
        data.data_name +
        '" :key="item.id" :value="item.id" >{{ item.nom }}</option> ';

      ma_rep = ma_rep + "  </select> ";
    }

    ma_rep = ma_rep + " </div>";
    return ma_rep;
  }
}

module.exports = search;
