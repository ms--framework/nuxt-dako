const capp = require("./app.js");
var clapp = new capp();

class search {
  constructor(module) {
    this.module = module;
  }

  genererate(nom_module, mydata_list, mydata) {
    //
    clapp.create_doc("./pages/" + nom_module + "/view");
    const new_template_1 = clapp.recup_file("./ms/template/view.vue");
    //

    var formulaire = "";
    var i = 0;
    for (i = 0; i < mydata.length; i++) {
      formulaire = formulaire + this.generate_html(mydata[i]);
    }
    var new_template_2 = new_template_1.replace("monnomdemodule", nom_module);
    var new_template_3 = new_template_2.replace("monnomdemodule", nom_module);
    var new_template_4 = new_template_3.replace(
      "<formulaire></formulaire>",
      formulaire
    );

    clapp.save_file("./pages/" + nom_module + "/view/_id.vue", new_template_4);
    //
  }

  generate_html(data) {
    var ma_rep =
      '<div class="form-group"><label for="">' + data.title + "</label>";

    if (data.type == "input") {
      ma_rep = ma_rep + " {{ data." + data.data_name + " }} ";
    }

    ma_rep = ma_rep + " </div>";
    return ma_rep;
  }
}

module.exports = search;
