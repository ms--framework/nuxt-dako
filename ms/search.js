const capp = require("../ms/app.js");
var clapp = new capp();

class search {
  constructor(module) {
    this.module = module;
  }

  genererate(nom_module, mydata_list, mydata) {
    //
    const template = clapp.recup_file("./ms/template/list.vue");
    //

    //
    var headtable = "";
    var headbody = "";
    var modalfiltre = "";
    //

    //
    var i = 0;
    for (i = 0; i < mydata.length; i++) {
      var bc = 0;
      for (bc = 0; bc < mydata_list.length; bc++) {
        if (mydata_list[bc]["id"] == mydata[i]["id"]) {
          headtable = headtable + "<th>" + mydata[i]["title"] + "</th>";
          headbody =
            headbody + "<th> {{ item." + mydata[i]["data_name"] + " }} </th>";
          modalfiltre = modalfiltre + this.generate_html(mydata[i]);
        }
      }
    }

    //
    var new_template_1 = template.replace("<headtable></headtable>", headtable);
    var new_template_2 = new_template_1.replace(
      "<headbody></headbody>",
      headbody
    );
    var new_template_3 = new_template_2.replace(
      "<modalfiltre></modalfiltre>",
      modalfiltre
    );
    var new_template_4 = new_template_3.replace("monnomdemodule", nom_module);
    var new_template_5 = new_template_4.replace("monnomdemodule", nom_module);
    var new_template_6 = new_template_5.replace("monnomdemodule", nom_module);
    var new_template_7 = new_template_6.replace("monnomdemodule", nom_module);
    //

    clapp.save_file("./pages/" + nom_module + "/search.vue", new_template_7);
  }

  generate_html(data) {
    var ma_rep =
      '<div class="form-group"><label for="">' + data.title + "</label>";

    if (data.type == "input") {
      ma_rep =
        ma_rep +
        '  <input type="text" class="form-control" v-model="search.' +
        data.data_name +
        '" placeholder="' +
        data.placeholder +
        '"> ';
    }

    ma_rep = ma_rep + " </div>";
    return ma_rep;
  }
}

module.exports = search;
