export default function({ store, redirect }) {
  if (store.state.auth == false) {
    return redirect("/login");
  }
}
