export default function({ store, redirect }) {
  if (store.state.auth == true) {
    return redirect("/");
  }
}
