import axios from "axios";
var querystring = require("querystring");
var qs = require("qs");

export default class app {
  constructor(store) {
    this.store = store;
    this.api = this.store.state.api_url;
    this.token = this.store.state.token;

    // Script de connection
    this.url_login = this.api + "user/admin/auth/";
    this.url_forget_mdp = this.api + "user/admin/forgot/";
    this.url_stat = this.api + "stat/"; // Return {number:int,name:string  }

    //
  }
  async login(user) {
    let url = this.url_login + querystring.stringify(user);
    const { data } = await axios.post(url);
    if (data.reponse == true) {
      return data.data;
    } else {
      console.log(url);
      throw new Error(data.message);
    }
  }

  async forgot(user) {
    let url = this.url_forget_mdp + querystring.stringify(user);
    const { data } = await axios.post(url);
    // console.log(data.reponse);
    if (data.reponse == true) {
      return data.data;
    } else {
      console.log(url);
      throw new Error(data.message);
    }
  }

  async stat() {
    let url = this.url_stat;

    if (url != "") {
      const { data } = await axios.post(url);
      // console.log(data.reponse);
      if (data.reponse == true) {
        return data.data;
      } else {
        console.log(url);
        throw new Error(data.message);
      }
    } else {
      throw new Error("Not stat url");
    }
  }
  async stats(type) {
    let url = this.api + "stats/" + type;

    if (url != "") {
      const { data } = await axios.post(url);
      // console.log(data.reponse);
      if (data.reponse == true) {
        return data.data;
      } else {
        console.log(url);
        throw new Error(data.message);
      }
    } else {
      throw new Error("Not stat url");
    }
  }
}
